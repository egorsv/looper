import yaml

from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim.lr_scheduler import StepLR


class Looper:
    def __init__(self,
                val_funcs,
                loss_fn,
                optimizer,
                scheduler,
                cuda = True,
                logger = None,
                early_stopping = None):

        self.cuda = cuda
        self.val_funcs = val_funcs
        self.loss_fn = loss_fn
        self.opt = optimizer
        self.iters = 0 # for logger
        self.scheduler = scheduler

        if not logger is None:
            self.logging = True
            self.logger = logger

        if not early_stopping is None:
            self.early_stop = True
            self.early_stopping = early_stopping


    def validate_age(self, model, test):

        """ probably will need to rewrite
            it is not general and made for calculating an error with age
        """

        model.eval()
        with torch.no_grad():

            outputs_list =  []
            labels_list = []
            ages_list = []

            num_batch = 0
            mean_loss = 0

            for batch in test:
                assert type(batch[0]) == torch.Tensor, 'Data should always go first'
                assert type(batch[1]) == torch.Tensor, 'Labels should always go second'
                if self.cuda:
                    data = batch[0].cuda()
                    labels = batch[1].cuda()
                ages = batch[2]

                output=model(data)

                loss =  self.loss_fn(output, labels)
                mean_loss +=loss.item()
                num_batch +=1

                outputs_list.append(output)
                labels_list.append(labels)
                ages_list.append(ages)

        self.logger.add_scalars({'loss_tes':{'y':mean_loss/ num_batch, 'x':self.iters}})
        for fun in self.val_funcs:
            fun.compute(outputs_list,
                        labels_list,
                        ages_list)

        for fun in self.val_funcs:
            ''' values - dict where key is name of chart and value is a dict with iteration\'s
                number (x key) and y key is it\s value'''

            # FIX: Move prints to logger
            print(f'{fun.name}, SCORE:  {fun.score}')
            self.logger.add_scalars({fun.name:{'y':fun.score, 'x':self.iters}})

            # If need to use one of val errors
            if fun.order == 'main':

                err = fun.score
        self.logger.close()

        return err


    def train_batch(self,
                    model,
                    train):

        model.train()

        mean_loss = 0
        num_batch = 0
        for batch in tqdm(train):

                assert type(batch[0]) == torch.Tensor, 'Data should always go first'
                assert type(batch[1]) == torch.Tensor, 'Labels should always go second'

                if self.cuda:
                    data = batch[0].cuda()
                    labels = batch[1].cuda()
                output = model(data)

                # compute loss
                loss = self.loss_fn(output, labels)
                num_batch +=1
                mean_loss +=loss.item()
                # clear previous gradients,
                # compute gradients of all variables wrt loss
                self.opt.zero_grad()
                loss.backward()
                # performs updates using calculated gradients
                self.opt.step()
                self.iters +=1
        return mean_loss/ num_batch


    def train(self,
              model,
              train,
              test,
              early_stopping = True,
              max_epochs = 100):

        for i in range(max_epochs):
                loss_err = self.train_batch(model, train)
                self.logger.add_scalars({'loss':{'y':loss_err, 'x':i}})
                print(f'Epoch: {i}| AVG  epoch loss :{loss_err}')
                vall_error = self.validate_age(model, test)
                self.scheduler.step()

                if self.early_stop:
                    # NOTE: stop by loss err can, best score - >  0
                    # if best score - > 1, remove minus
                    self.early_stopping(-loss_err,
                                        model,
                                        i)
                    if self.early_stopping.early_stop:
                        print("Early stopping")
                        break
