from PIL import Image
import albumentations
from torch.utils.data import Dataset
from torchvision import datasets, transforms

# NOTE: Rename or set another name for age, group 


class DictLoader(Dataset):
    """ Input data: list of dicts path, label, age  
        Returns: tensor, label, age """

    def __init__(self, data, transforms=None, return_age=True):
        self.data = data
        self.transform = transforms
        self.return_age = return_age

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        img_path = self.data[idx]['path']
        label = self.data[idx]['label']
        #img = None
        if self.return_age:
            age = self.data[idx]['age']
        try:
            img = Image.open(img_path)
            img = img.convert("RGB")
            if self.transform:
                if (isinstance(self.transform, albumentations.core.composition.Compose)):
                    array = np.asarray(img)
                    img = self.transform(image=array)
                    img = img['image']
                    totensor = transforms.ToTensor()
                    img = totensor(img)
                else:
                    img = self.transform(img)
            
            return img, label, age
                
        except Exception as e:
            print(f'Error: {e}, \n Image path: {img_path}')
        
        
        