import torch


def load_resnext(num_outputs,
                 cuda =True,
                 weights_path = ''):

    model = torch.hub.load('pytorch/vision:v0.4.2', 'resnext50_32x4d', pretrained=False)

    model.fc = torch.nn.Linear(2048, num_outputs)

    if weights_path != '':
        model.load_state_dict(torch.load(weights_path))

    print('Loaded resnext50_32x4d')

    if cuda:
        model.cuda()
    return model
