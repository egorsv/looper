import torchvision
from torchvision import datasets, models, transforms
from albumentations import (
    HorizontalFlip, IAAPerspective, ShiftScaleRotate, CLAHE, RandomRotate90,
    Transpose, ShiftScaleRotate, Blur, OpticalDistortion, GridDistortion, HueSaturationValue,
    IAAAdditiveGaussianNoise, GaussNoise, MotionBlur, MedianBlur, RandomBrightnessContrast, IAAPiecewiseAffine,
    IAASharpen, IAAEmboss, Flip, OneOf, Compose, RandomCrop, Resize,CenterCrop
)


def augmentation_simple(image_size):
    return transforms.Compose([
        transforms.Resize(image_size),
        transforms.ToTensor()])


def augmentation_standart_xception(image_size=(299,299)):
    return torchvision.transforms.Compose([
        torchvision.transforms.Resize(image_size),
        torchvision.transforms.RandomHorizontalFlip(),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(
            [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]),
    ])


def augmentation_standart(image_size):
    return torchvision.transforms.Compose([
        torchvision.transforms.Resize(image_size),
        torchvision.transforms.RandomHorizontalFlip(),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(
            [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
    ])


def cropping_augmentation(image_size):
    return Compose([
        Resize(image_size+100, image_size+100, p=1.0),
        OneOf([
            CenterCrop(image_size, image_size, p=0.5),
            Resize(image_size, image_size, p=0.5),
        ], p=1),
        CLAHE(p=1)
    ], p=1.0)


def augmentation_clahe(image_size):
    return Compose([
        Resize(image_size, image_size, p=1.0),
        CLAHE(p=1)
    ], p=1)


def augmentation_clahe_hard(image_size):
    return Compose([
        Resize(image_size, image_size, p=1.0),
        CLAHE(clip_limit=60, tile_grid_size=(15, 15), p=1)
    ], p=1)


def augmentation_contrast1(image_size):
    return Compose([
        OneOf([
            RandomCrop(image_size, image_size),
            Resize(image_size, image_size)], p=1.0),
        OneOf([
            IAAAdditiveGaussianNoise(),
            GaussNoise(),
        ], p=0.2),
        OneOf([
            CLAHE(clip_limit=2),
            RandomBrightnessContrast(),
        ], p=0.3),
        HueSaturationValue(p=0.3),
    ], p=1.0)


def augmentation_image(image_size):
    return Compose([
        Resize(image_size+100, image_size+100, p=1.0),
        CLAHE(clip_limit=2, p=1.0),
        OneOf([
            CenterCrop(image_size, image_size, p=0.5),
            Resize(image_size, image_size, p=0.5),
        ], p=1),
        OneOf([
            IAAAdditiveGaussianNoise(),
            GaussNoise(),
        ], p=0.3),
        OneOf([
            CLAHE(clip_limit=2, p=0.7),
            IAASharpen(),
            IAAEmboss(),
            RandomBrightnessContrast(p=0.6),
        ], p=0.5),
    ], p=1)

def augmentation_contrast2(image_size):
    return Compose([
        RandomCrop(image_size, image_size),
        Flip(),
        OneOf([
            IAAAdditiveGaussianNoise(),
            GaussNoise(),
        ], p=0.2),
        OneOf([
            MotionBlur(p=.2),
            MedianBlur(blur_limit=3, p=0.1),
            Blur(blur_limit=3, p=0.1),
        ], p=0.2),
        ShiftScaleRotate(shift_limit=0.0625, scale_limit=0.2, rotate_limit=45, p=0.2),
        OneOf([
            OpticalDistortion(p=0.3),
            GridDistortion(p=.1),
            IAAPiecewiseAffine(p=0.3),
        ], p=0.2),
        OneOf([
            CLAHE(clip_limit=2),
            IAASharpen(),
            IAAEmboss(),
            RandomBrightnessContrast(),
        ], p=0.3),
        HueSaturationValue(p=0.3),
    ], p=0.5)
