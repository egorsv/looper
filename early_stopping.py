import numpy as np
import torch
import os

class EarlyStopping:
    """Early stops the training if validation loss doesn't improve after a given patience."""

    def __init__(self,
                 output_file,
                 patience=7,
                 verbose=False,
                 start_from = 30):
        """
        Args:
            patience (int): How long to wait after last time validation loss improved.
                            Default: 7
            verbose (bool): If True, prints a message for each validation loss improvement.
                            Default: False
            start_from (float): Minimum epochs to start using early stopping.
                            Default: 0
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf
        self.output_file = output_file
        self.start_from =  start_from
        self.model_path = output_file

    def __call__(self, val_loss, model, epoch):

        # start epoch afetr "start_from"

        if epoch > self.start_from:

            score = val_loss
            print(f'Early stopper: current score: {score}, Bestscore {self.best_score} ')
            if self.best_score is None:
                self.best_score = score
                self.save_checkpoint(val_loss, model)

            if  score < self.best_score:
                self.counter += 1
                if self.counter == 1:
                    self.model_path='_'.join([self.output_file, "patience", str(self.patience), str(score)])
                    self.save_checkpoint(val_loss, model)
                print(f'EarlyStopping counter: {self.counter} out of {self.patience}')
                if self.counter >= self.patience:
                    self.early_stop = True
            else:
                self.best_score > score
                self.best_score = score
                #self.save_checkpoint(val_loss, model)
                self.counter = 0

    def save_checkpoint(self, val_loss, model):
        '''Saves model when validation loss decrease.'''
        if self.verbose:
            print(f'Validation roc auc increased ({self.val_loss_min:.6f} --> {val_loss:.6f}).  Saving model ...')
        torch.save(model.state_dict(), self.model_path)
        self.val_loss_min = val_loss
