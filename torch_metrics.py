import numpy as np
import torch.nn as nn
import matplotlib.pyplot as plt
from sklearn.metrics import (precision_recall_curve,
                             roc_auc_score,
                            average_precision_score,
                            auc,
                            accuracy_score)


class Metrics:

    """ All methods accept lists of items  """

    def __init__(self):
        self.score = 0
        self.order = 'secondary'
        self.soft = nn.Softmax(1)


    def to_np(self, items):
        return [arr.detach().cpu().numpy() for arr in items]


    def soften(self, items):
        result = []
        for item in items:
            if item.dim()==1:
                item = item.reshape(1, item.shape[0])
            result.append(self.soft(item))
        return result

    def prepare_for_curves(self, arrays):
        """
        Input: list of torch tensors
        Applies soft max and returns one numpy array
        """

        # Normalize to make probs

        arrays = self.soften(arrays)
        arrays = self.to_np(arrays)
        # return only probs for 1st class
        # NOTE: actually average_precision_score can work with multiclass
        # NOTE: fix later
        return np.vstack(arrays)[:,1]

class Accuracy(Metrics):

    """ Accepts a  list of row torch tensors
        Labels and outputs """

    def __init__(self):
        Metrics.__init__(self)
        self.name = 'accuracy'

    def compute(self, outputs, labels, *args):
        correct = 0
        total = 0

        outputs = self.to_np(outputs)
        labels = self.to_np(labels)

        for output, label in zip(outputs, labels):
            output = output
            label = label
            predicted = np.argmax(output, 1)
            total += len(label)
            correct +=(predicted == label).sum()

        self.score = correct / total
        return self.score

class PRscore(Metrics):
    """ Accepts a list of row torch tensors:
        Layer outputs and labels.
    """

    def __init__(self, logger = None, name = 'pr_auc'):
        Metrics.__init__(self)
        # logger tensorboard Summary Writer
        self.name = name
        self.logger = logger

    def prepare_inputs(self, outputs, labels):
        outputs = self.prepare_for_curves(outputs)
        labels = np.hstack(self.to_np(labels))
        return outputs, labels

        # Transform torch tensors to np arrays and concatenate them
    def compute(self, outputs, labels, *args):
        outputs, labels = self.prepare_inputs(outputs, labels)

        if self.logger is not None:
            self.logger.add_pr_curve(self.name,
                                     labels,
                                     outputs,
                                     global_step=None,
                                     num_thresholds=500) # Num of steps to draw treshold

        if len(np.unique(labels)) == 1:
            # NOTE check classes
            print('ERR pr_score_score - Only one class present in y_true')
            outputs[outputs > 0.5] = 1
            outputs[outputs <= 0.5] = 0
            self.score = accuracy_score(labels, outputs)
        else:
            self.score = average_precision_score(labels, outputs)

        return self.score


class ROCauc(Metrics):
    """ Accepts a list of row torch tensors:
        Layer outputs and labels.
    """

    def __init__(self):
        Metrics.__init__(self)
        self.name = 'roc_auc'

    def prepare_inputs(self, outputs, labels):
        outputs = self.prepare_for_curves(outputs)
        labels = np.hstack(self.to_np(labels))
        return outputs, labels

        # Transform torch tensors to np arrays and concatenate them
    def compute(self, outputs, labels, *args):
        outputs, labels = self.prepare_inputs(outputs, labels)
        #NOTE: to avoid ERR roc_auc_score - Only one class present in y_true

        if len(np.unique(labels)) == 1:
            # NOTE check classes
            print('ERR roc_auc_score - Only one class present in y_true')
            outputs[outputs > 0.5] = 1
            outputs[outputs <= 0.5] = 0
            self.score = accuracy_score(labels, outputs)
        else:
            self.score = roc_auc_score(labels, outputs)

        return self.score



def compute_group(func,
                  outputs,
                  labels,
                  group_labels,
                  group):

    filtered_outputs = []
    filtered_labels = []
    for group_label, output, label in zip(group_labels, outputs, labels):
        new_out = output[group_label == group]
        new_label = label[group_label == group]
        if new_out.shape[0] == 0:
            continue
        filtered_outputs.append(new_out)
        filtered_labels.append(new_label)
    #If small batch
    if len(filtered_outputs) > 1:
        score = func.compute(filtered_outputs, filtered_labels)
    else:
        #NOTE:  Maybe change it later
        score = 0.5
    return score


class ROCauc_by_group():
    """ Accepts a list of row torch tensors:
        Layer outputs and labels.
    """

    def __init__(self, group_num, group_name=None):
        Metrics.__init__(self)
        self.group = group_num

        if group_name is None:
            group_name = group_num

        self.name = f'ROCauc_by_group_{group_name}'
        self.func = ROCauc()

    def compute(self, outputs, labels, group_labels, *args):
        self.score = compute_group(self.func,
                                   outputs,
                                   labels,
                                   group_labels,
                                   self.group)
        return self.score


class PRauc_by_group():
    """ Accepts a list of row torch tensors:
        Layer outputs and labels.
    """

    def __init__(self,
                 group_num,
                 group_name=None,
                 logger=None):

        Metrics.__init__(self)
        self.group = group_num
        self.logger = logger

        if group_name is None:
            group_name = group_num

        self.name = f'PRauc_by_group_{group_name}'
        self.func = PRscore(self.logger, self.name)

    def compute(self, outputs, labels, group_labels, *args):
        self.score = compute_group(self.func,
                                   outputs,
                                   labels,
                                   group_labels,
                                   self.group)
        return self.score
